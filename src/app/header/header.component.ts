import { Component,EventEmitter,Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent {
	@Output() onFeatureSelected = new EventEmitter<string>();
	select(feature:string) {
		this.onFeatureSelected.emit(feature);
	}
}
